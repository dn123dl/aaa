'use strict'

window.$ = window.jQuery = require('jquery');
window.Bootstrap = require('bootstrap');

var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://iot.eclipse.org')

client.on('connect', function () {
  client.subscribe('testRfidTopic', function (err) {
    if (!err) {
      client.publish('testRfidTopic', 'Hello mqtt')
    }
  })
})

client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
  alert(message.toString())
  //client.end()
})
